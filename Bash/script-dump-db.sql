I. Import
-----SELECT SESSION FOR KILL
SELECT s.sid, s.serial#, s.status, p.spid FROM v$session s, v$process p WHERE s.username = 'ED_HQHN_CAL' AND p.addr(+) = s.paddr;
 /
Pass actual SID and SERIAL# values for user TEST then drop user...:

ALTER SYSTEM KILL SESSION '<SID>, <SERIAL>'
/

0.5----------------------drop user
DROP USER ED_HQHN_CAL CASCADE;
DROP USER ED_HQHN_CFG CASCADE;
DROP USER ED_HQHN_CTL CASCADE;
DROP USER ED_HQHN_DMS CASCADE;
DROP USER ED_HQHN_NOTi CASCADE;
DROP USER ED_HQHN_RPT CASCADE;
DROP USER ED_HQHN_UMS CASCADE;
DROP USER ED_HQHN_WFS CASCADE;

1----------------------
CREATE USER ED_HQHN_CAL IDENTIFIED BY abc123;
GRANT ALL PRIVILEGES TO ED_HQHN_CAL;
GRANT UNLIMITED TABLESPACE TO ED_HQHN_CAL;
GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO ED_HQHN_CAL;

2-----------------------
CREATE USER ED_HQHN_CFG IDENTIFIED BY abc123;
GRANT ALL PRIVILEGES TO ED_HQHN_CFG;
GRANT UNLIMITED TABLESPACE TO ED_HQHN_CFG;
GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO ED_HQHN_CFG;

3-----------------------
CREATE USER ED_HQHN_CTL IDENTIFIED BY abc123;
GRANT ALL PRIVILEGES TO ED_HQHN_CTL;
GRANT UNLIMITED TABLESPACE TO ED_HQHN_CTL;
GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO ED_HQHN_CTL;

4-----------------------
CREATE USER ED_HQHN_DMS IDENTIFIED BY abc123;
GRANT ALL PRIVILEGES TO ED_HQHN_DMS;
GRANT UNLIMITED TABLESPACE TO ED_HQHN_DMS;
GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO ED_HQHN_DMS;

5-----------------------
CREATE USER ED_HQHN_NOTi IDENTIFIED BY abc123;
GRANT ALL PRIVILEGES TO ED_HQHN_NOTi;
GRANT UNLIMITED TABLESPACE TO ED_HQHN_NOTi;
GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO ED_HQHN_NOTi;

6-----------------------
CREATE USER ED_HQHN_RPT IDENTIFIED BY abc123;
GRANT ALL PRIVILEGES TO ED_HQHN_RPT;
GRANT UNLIMITED TABLESPACE TO ED_HQHN_RPT;
GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO ED_HQHN_RPT;

7-----------------------
CREATE USER ED_HQHN_UMS IDENTIFIED BY abc123;
GRANT ALL PRIVILEGES TO ED_HQHN_UMS;
GRANT UNLIMITED TABLESPACE TO ED_HQHN_UMS;
GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO ED_HQHN_UMS;

8-----------------------
CREATE USER ED_HQHN_WFS IDENTIFIED BY abc123;
GRANT ALL PRIVILEGES TO ED_HQHN_WFS;
GRANT UNLIMITED TABLESPACE TO ED_HQHN_WFS;
GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO ED_HQHN_WFS;

9-------------CMS?????

---- =now DUMP_TIME=`date +%Y%m%d_%H%M`

DUMP_TIME=20220401_0922
----- IMPDP
impdp ED_HQHN_CAL/abc123@pdb1  DIRECTORY=DATA_PUMP_DIR  dumpfile=ED_HQHN_CAL_$DUMP_TIME.dmp  logfile=impdp_ED_HQHN_CAL_$DUMP_TIME.log;
impdp ED_HQHN_CFG/abc123@pdb1  DIRECTORY=DATA_PUMP_DIR  dumpfile=ED_HQHN_CFG_$DUMP_TIME.dmp  logfile=impdp_ED_HQHN_CFG_$DUMP_TIME.log;
impdp ED_HQHN_CTL/abc123@pdb1  DIRECTORY=DATA_PUMP_DIR  dumpfile=ED_HQHN_CTL_$DUMP_TIME.dmp  logfile=impdp_ED_HQHN_CTL_$DUMP_TIME.log;
impdp ED_HQHN_DMS/abc123@pdb1  DIRECTORY=DATA_PUMP_DIR  dumpfile=ED_HQHN_DMS_$DUMP_TIME.dmp  logfile=impdp_ED_HQHN_DMS_$DUMP_TIME.log;
impdp ED_HQHN_NOTi/abc123@pdb1 DIRECTORY=DATA_PUMP_DIR  dumpfile=ED_HQHN_NOTi_$DUMP_TIME.dmp logfile=impdp_ED_HQHN_NOTi_$DUMP_TIME.log;
impdp ED_HQHN_RPT/abc123@pdb1  DIRECTORY=DATA_PUMP_DIR  dumpfile=ED_HQHN_RPT_$DUMP_TIME.dmp  logfile=impdp_ED_HQHN_RPT_$DUMP_TIME.log;
impdp ED_HQHN_UMS/abc123@pdb1  DIRECTORY=DATA_PUMP_DIR  dumpfile=ED_HQHN_UMS_$DUMP_TIME.dmp  logfile=impdp_ED_HQHN_UMS_$DUMP_TIME.log;
impdp ED_HQHN_WFS/abc123@pdb1  DIRECTORY=DATA_PUMP_DIR  dumpfile=ED_HQHN_WFS_$DUMP_TIME.dmp  logfile=impdp_ED_HQHN_WFS_$DUMP_TIME.log;
--??
impdp ED_CMS_HQ/abc123@tchq  DIRECTORY=DATA_PUMP_DIR  dumpfile=CMC_TS_CMS_$DUMP_TIME.dmp remap_schema=CMC_TS_CMS:ED_CMS_HQ logfile=impdp_CMC_TS_CMS_$DUMP_TIME.log;

II. Export

/u01/docker/oracle19c/admin/CDB/dpdump/DB556AAF374F0CD6E053020012AC8C45/
DUMP_TIME=
-----EXPDP
expdp \"sys/Abcd1234@PDB1 as SYSDBA\" schemas=ED_HQHN_CAL directory=DATA_PUMP_DIR dumpfile=ED_HQHN_CAL_$DUMP_TIME.dmp logfile=expdp_ED_HQHN_CAL_$DUMP_TIME.log;
expdp \"sys/Abcd1234@EDOCTC as SYSDBA\" schemas=ED_HQHN_CFG directory=DATA_PUMP_DIR dumpfile=ED_HQHN_CFG_$DUMP_TIME.dmp logfile=expdp_ED_HQHN_CFG_$DUMP_TIME.log;
expdp \"sys/Abcd1234@EDOCTC as SYSDBA\" schemas=ED_HQHN_CTL directory=DATA_PUMP_DIR dumpfile=ED_HQHN_CTL_$DUMP_TIME.dmp logfile=expdp_ED_HQHN_CTL_$DUMP_TIME.log;
expdp \"sys/Abcd1234@EDOCTC as SYSDBA\" schemas=ED_HQHN_DMS directory=DATA_PUMP_DIR dumpfile=ED_HQHN_DMS_$DUMP_TIME.dmp logfile=expdp_ED_HQHN_DMS_$DUMP_TIME.log;
expdp \"sys/Abcd1234@EDOCTC as SYSDBA\" schemas=ED_HQHN_NOTi directory=DATA_PUMP_DIR dumpfile=ED_HQHN_NOTi_$DUMP_TIME.dmp logfile=expdp_ED_HQHN_NOTi_$DUMP_TIME.log;
expdp \"sys/Abcd1234@EDOCTC as SYSDBA\" schemas=ED_HQHN_RPT directory=DATA_PUMP_DIR dumpfile=ED_HQHN_RPT_$DUMP_TIME.dmp logfile=expdp_ED_HQHN_RPT_$DUMP_TIME.log;
expdp \"sys/Abcd1234@EDOCTC as SYSDBA\" schemas=ED_HQHN_UMS directory=DATA_PUMP_DIR dumpfile=ED_HQHN_UMS_$DUMP_TIME.dmp logfile=expdp_ED_HQHN_UMS_$DUMP_TIME.log;
expdp \"sys/Abcd1234@EDOCTC as SYSDBA\" schemas=ED_HQHN_WFS directory=DATA_PUMP_DIR dumpfile=ED_HQHN_WFS_$DUMP_TIME.dmp logfile=expdp_ED_HQHN_WFS$DUMP_TIME.log;
--??
expdp \"sys/Abcd1234@EDOCTC as SYSDBA\" schemas=ED_CMS_HQ directory=DATA_PUMP_DIR dumpfile=ED_CMS_HQ_$DUMP_TIME.dmp logfile=expdp_ED_CMS_HQ_$DUMP_TIME.log;
