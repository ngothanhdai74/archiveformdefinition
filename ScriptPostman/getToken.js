let bodyRequest = JSON.parse(pm.variables.get("edocadmin"));
let apiGateWay = pm.variables.get("APIGateway");
let pathLogin = "edoc/ums/account/login";
let urlLogin = `${apiGateWay}${pathLogin}`;
pm.sendRequest({
    url: urlLogin,
    method: "POST",
    body: {
        mode: 'raw',
        raw: JSON.stringify(bodyRequest)
    },
    header: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
}, function(err, res) {
    let response = res.json();
    console.log('daint', response.AccessToken);
    pm.environment.set("AccessToken", response.AccessToken);
});